@props(['mq'=>'md','s'=>'6'])
<div class="col-{{$mq}}-{{$s}} {{$attributes['class']}}">
    {{$slot}}
</div>
