@props(['label'=>'','name'=>'','s'=>'12'])
<div class="row">
    <div class="col-sm-{{$s}}">
        <div class="form-group">
            <label>{{$label}}</label>
            {{$slot}}
        </div>
    </div>
</div>